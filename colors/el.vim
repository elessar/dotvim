" black:       [234, '#1c1c1c']
" darkred:     [131, '#af5f5f']
" darkgreen:   [65,  '#5f875f']
" darkyellow:  [101, '#87875f']
" darkblue:    [67,  '#5f87af']
" darkmagenta: [60,  '#5f5f87']
" darkcyan:    [66,  '#5f8787']
" lightgrey:   [248, '#a8a8a8']
" darkgrey:    [240, '#585858']
" red:         [167, '#d75f5f']
" green:       [108, '#87af87']
" yellow:      [229, '#ffffaf']
" blue:        [110, '#87afd7']
" magenta:     [103, '#8787af']
" cyan:        [73,  '#5fafaf']
" white:       [15,  '#ffffff']

set background=dark
highlight clear
if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'el'

if &t_Co == 8
  set t_Co=16
endif

let s:has_colors = (&t_Co >= 256 || has('gui_running') ||
      \            (has('termguicolors') && &termguicolors))

if s:has_colors
  highlight Normal cterm=NONE gui=NONE ctermfg=15 ctermbg=235 guifg=#ffffff guibg=#262626
  highlight ColorColumn cterm=NONE gui=NONE ctermfg=NONE ctermbg=234 guifg=NONE guibg=#1c1c1c
  highlight CursorColumn cterm=NONE gui=NONE ctermfg=NONE ctermfg=234 guifg=NONE guibg=#1c1c1c
  highlight CursorLine cterm=NONE gui=NONE ctermfg=NONE ctermbg=234 guifg=NONE guibg=#1c1c1c
  highlight Ignore cterm=NONE gui=NONE ctermfg=bg ctermbg=NONE guifg=bg guibg=NONE
  highlight Cursor cterm=NONE gui=NONE ctermfg=bg ctermbg=248 guifg=bg guibg=#a8a8a8
  highlight Comment cterm=NONE gui=NONE ctermfg=60 ctermbg=NONE guifg=#5f5f87 guibg=NONE
  highlight String cterm=NONE gui=NONE ctermfg=248 ctermbg=NONE guifg=#a8a8a8 guibg=NONE
  highlight EndOfBuffer cterm=NONE gui=NONE ctermfg=240 ctermbg=NONE guifg=#585858 guibg=NONE
  highlight Folded cterm=NONE gui=NONE ctermfg=240 ctermbg=NONE guifg=#585858 guibg=NONE
  highlight FoldColumn cterm=NONE gui=NONE ctermfg=240 ctermbg=NONE guifg=#585858 guibg=NONE
  highlight NonText cterm=NONE gui=NONE ctermfg=240 ctermbg=NONE guifg=#585858 guibg=NONE
  highlight SpecialKey cterm=NONE gui=NONE ctermfg=240 ctermbg=NONE guifg=#585858 guibg=NONE
  highlight Visual cterm=reverse gui=reverse ctermfg=240 ctermbg=234 guifg=#585858 guibg=#1c1c1c
  highlight VertSplit cterm=NONE gui=NONE ctermfg=240 ctermbg=240 guifg=#585858 guibg=#585858
  highlight ErrorChar cterm=NONE gui=NONE ctermfg=167 ctermbg=NONE guifg=#d75f5f guibg=NONE
  highlight ErrorMsg cterm=NONE gui=NONE ctermfg=167 ctermbg=NONE guifg=#d75f5f guibg=NONE
  highlight Error cterm=NONE gui=NONE ctermfg=167 ctermbg=234 guifg=#d75f5f guibg=#1c1c1c
  highlight MoreMsg cterm=NONE gui=NONE ctermfg=108 ctermbg=NONE guifg=#87af87 guibg=NONE
  highlight Question cterm=NONE gui=NONE ctermfg=108 ctermbg=NONE guifg=#87af87 guibg=NONE
  highlight Todo cterm=NONE gui=NONE ctermfg=229 ctermbg=NONE guifg=#ffffaf guibg=NONE
  highlight WarningMsg cterm=NONE gui=NONE ctermfg=229 ctermbg=NONE guifg=#ffffaf guibg=NONE
  highlight ModeMsg cterm=NONE gui=NONE ctermfg=73 ctermbg=NONE guifg=#5fafaf guibg=NONE
  highlight Directory cterm=NONE gui=NONE ctermfg=73 ctermbg=NONE guifg=#5fafaf guibg=NONE
  highlight MatchParen cterm=NONE gui=NONE ctermfg=110 ctermbg=NONE guifg=#87afd7 guibg=NONE

  highlight StatusLine cterm=NONE gui=NONE ctermfg=234 ctermbg=248 guifg=#1c1c1c guibg=#a8a8a8
  highlight StatusLineNC cterm=NONE gui=NONE ctermfg=234 ctermbg=240 guifg=#1c1c1c guibg=#585858
  highlight WildMenu cterm=NONE gui=NONE ctermfg=234 ctermbg=15 guifg=#1c1c1c guibg=#ffffff
  highlight StatusLineTerm cterm=NONE gui=NONE ctermfg=234 ctermbg=103 guifg=#1c1c1c guibg=#8787af
  highlight StatusLineTermNC cterm=NONE gui=NONE ctermfg=234 ctermbg=60 guifg=#1c1c1c guibg=#5f5f87
  highlight TabLine cterm=NONE gui=NONE ctermfg=234 ctermbg=248 guifg=#1c1c1c guibg=#a8a8a8
  highlight TabLineFill cterm=NONE gui=NONE ctermfg=234 ctermbg=248 guifg=#1c1c1c guibg=#a8a8a8
  highlight TabLineSel cterm=NONE gui=NONE ctermfg=15 ctermbg=NONE guifg=#ffffff guibg=NONE
  highlight LineNr cterm=NONE gui=NONE ctermfg=240 ctermbg=NONE guifg=#585858 guibg=NONE
  highlight SignColumn cterm=NONE gui=NONE ctermfg=240 ctermbg=NONE guifg=#585858 guibg=NONE
  highlight CursorLineNr cterm=NONE gui=NONE ctermfg=15 ctermbg=NONE guifg=#ffffff guibg=NONE
  highlight Search cterm=reverse gui=reverse ctermfg=67 ctermbg=234 guifg=#5f87af guibg=#1c1c1c
  highlight IncSearch cterm=reverse gui=reverse ctermfg=73 ctermbg=234 guifg=#5fafaf guibg=#1c1c1c
  highlight DiffDelete cterm=NONE gui=NONE ctermfg=131 ctermbg=NONE guifg=#af5f5f guibg=NONE
  highlight diffRemoved cterm=NONE gui=NONE ctermfg=131 ctermbg=NONE guifg=#af5f5f guibg=NONE
  highlight DiffAdd cterm=NONE gui=NONE ctermfg=65 ctermbg=NONE guifg=#5f875f guibg=NONE
  highlight diffAdded cterm=NONE gui=NONE ctermfg=65 ctermbg=NONE guifg=#5f875f guibg=NONE
  highlight DiffText cterm=reverse gui=reverse ctermfg=60 ctermbg=NONE guifg=#5f5f87 guibg=NONE
  highlight diffSubname cterm=NONE gui=NONE ctermfg=66 ctermbg=NONE guifg=#5f8787 guibg=NONE
  highlight diffLine cterm=NONE gui=NONE ctermfg=73 ctermbg=NONE guifg=#5fafaf guibg=NONE
  highlight Pmenu cterm=NONE gui=NONE ctermfg=NONE ctermbg=240 guifg=NONE guibg=#585858
  highlight PmenuSbar cterm=NONE gui=NONE ctermfg=NONE ctermbg=240 guifg=NONE guibg=#585858
  highlight SpellBad cterm=NONE gui=NONE ctermfg=131 ctermbg=NONE guifg=#af5f5f guibg=NONE
  highlight SpellCap cterm=NONE gui=NONE ctermfg=67 ctermbg=NONE guifg=#5f87af guibg=NONE
  highlight SpellLocal cterm=NONE gui=NONE ctermfg=66 ctermbg=NONE guifg=#5f8787 guibg=NONE
  highlight SpellRare cterm=NONE gui=NONE ctermfg=66 ctermbg=NONE guifg=#5f8787 guibg=NONE

  highlight Conceal cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight Constant cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight Function cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight Identifier cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight PreProc cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight Special cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight Statement cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight Title cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight Type cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE

  highlight DiffChange cterm=NONE gui=NONE ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight PmenuSel cterm=reverse gui=reverse ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
  highlight PmenuThumb cterm=reverse gui=reverse ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
else
  highlight Normal cterm=NONE ctermfg=white ctermbg=black
  highlight CursorLine cterm=NONE,underline ctermfg=darkgray ctermbg=NONE
  highlight ColorColumn cterm=NONE ctermfg=NONE ctermbg=red
  highlight CursorColumn cterm=NONE ctermfg=NONE ctermbg=darkgrey
  highlight Ignore cterm=NONE ctermfg=bg ctermbg=NONE
  highlight Cursor cterm=NONE ctermfg=bg ctermbg=lightgrey
  highlight Comment cterm=NONE ctermfg=darkmagenta ctermbg=NONE
  highlight String cterm=NONE ctermfg=lightgrey ctermbg=NONE
  highlight EndOfBuffer cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight Folded cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight FoldColumn cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight NonText cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight SpecialKey cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight Visual cterm=reverse ctermfg=black ctermbg=darkgray
  highlight VertSplit cterm=NONE ctermfg=darkgrey ctermbg=darkgrey
  highlight ErrorChar cterm=NONE ctermfg=red ctermbg=NONE
  highlight ErrorMsg cterm=NONE ctermfg=red ctermbg=NONE
  highlight Error cterm=NONE ctermfg=red ctermbg=black
  highlight MoreMsg cterm=NONE ctermfg=green ctermbg=NONE
  highlight Question cterm=NONE ctermfg=green ctermbg=NONE
  highlight Todo cterm=NONE ctermfg=yellow ctermbg=NONE
  highlight WarningMsg cterm=NONE ctermfg=yellow ctermbg=NONE
  highlight ModeMsg cterm=NONE ctermfg=cyan ctermbg=NONE
  highlight Directory cterm=NONE ctermfg=cyan ctermbg=NONE
  highlight MatchParen cterm=NONE ctermfg=blue ctermbg=NONE

  highlight StatusLine cterm=NONE ctermfg=black ctermbg=lightgrey
  highlight StatusLineNC cterm=NONE ctermfg=black ctermbg=darkgrey
  highlight WildMenu cterm=NONE ctermfg=black ctermbg=white
  highlight StatusLineTerm cterm=NONE ctermfg=black ctermbg=magenta
  highlight StatusLineTermNC cterm=NONE ctermfg=black ctermbg=darkmagenta
  highlight TabLine cterm=NONE ctermfg=black ctermbg=lightgrey
  highlight TabLineFill cterm=NONE ctermfg=black ctermbg=lightgrey
  highlight TabLineSel cterm=NONE ctermfg=white ctermbg=NONE
  highlight LineNr cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight SignColumn cterm=NONE ctermfg=darkgrey ctermbg=NONE
  highlight CursorLineNr cterm=NONE ctermfg=white ctermbg=NONE
  highlight Search cterm=reverse ctermfg=cyan ctermbg=bg
  highlight IncSearch cterm=reverse ctermfg=cyan ctermbg=black
  highlight DiffDelete cterm=NONE ctermfg=darkred ctermbg=NONE
  highlight diffRemoved cterm=NONE ctermfg=darkred ctermbg=NONE
  highlight DiffAdd cterm=NONE ctermfg=darkgreen ctermbg=NONE
  highlight diffAdded cterm=NONE ctermfg=darkgreen ctermbg=NONE
  highlight DiffText cterm=reverse ctermfg=darkmagenta ctermbg=NONE
  highlight diffSubname cterm=NONE ctermfg=darkcyan ctermbg=NONE
  highlight diffLine cterm=NONE ctermfg=cyan ctermbg=NONE
  highlight Pmenu cterm=NONE ctermfg=NONE ctermbg=darkgrey
  highlight PmenuSbar cterm=NONE ctermfg=NONE ctermbg=darkgrey
  highlight SpellBad cterm=NONE ctermfg=darkred ctermbg=NONE
  highlight SpellCap cterm=NONE ctermfg=darkblue ctermbg=NONE
  highlight SpellLocal cterm=NONE ctermfg=darkcyan ctermbg=NONE
  highlight SpellRare cterm=NONE ctermfg=darkcyan ctermbg=NONE

  highlight Conceal cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight Constant cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight Function cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight Identifier cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight PreProc cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight Special cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight Statement cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight Title cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight Type cterm=NONE ctermfg=NONE ctermbg=NONE

  highlight DiffChange cterm=NONE ctermfg=NONE ctermbg=NONE
  highlight PmenuSel cterm=reverse ctermfg=NONE ctermbg=NONE
  highlight PmenuThumb cterm=reverse ctermfg=NONE ctermbg=NONE
endif

highlight! link gitcommitOverflow ErrorMsg
