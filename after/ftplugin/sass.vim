setlocal iskeyword+=-
setlocal shiftwidth=2
setlocal suffixesadd+=.sss

nnoremap <silent><buffer> \w :<c-u>botright term ++rows=4 ++close make watch<cr><c-w>:set winfixheight<cr><c-w>p

let b:undo_ftplugin .= '|setl isk< sw< sua<|nunmap<buffer>\w'
