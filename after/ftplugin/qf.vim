setlocal statusline=%q%(\ %{getwinvar(0,'quickfix_title')}%)%=\ %l/%L

setlocal nocursorline

wincmd J

let b:undo_ftplugin .= '|setl stl< cul<'
