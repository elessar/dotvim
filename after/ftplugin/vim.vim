setlocal foldmethod=marker
setlocal keywordprg=:help
let &l:define = '\v^\s*(fu%[nction]!?\s+([gs]:)?|' .
      \ 'com%[mand]!?(\s+-\S+)*\s+\ze\u|' .
      \ 'aug%[roup]\s+(\cend)@!\ze\S)'
setlocal shiftwidth=2
setlocal fileformat=unix

let b:undo_ftplugin .= '|setl fdm< kp< def< sw< ff<'

function! s:AutoloadPaths(range)
  let l:path = expand('%:p')
  if l:path !~? '[/\\]autoload[/\\]'
    return
  endif
  call custom#Preserve(a:range.'s/\v^\s*fu%[nction]!?\s+\zs.*\ze#/'.
        \ tr(matchstr(l:path, 'autoload[/\\]\zs.*\ze.vim'), '/\', '##').'/e')
endfunction
command! -buffer -range=% AutoloadPaths
      \ call s:AutoloadPaths('<line1>,<line2>')

command! -buffer SortOptions
      \ call custom#Preserve('''{,''}- sort /\v^\s*se%[tglobal]\s+(no)=/')
