setlocal statusline=%f%=\ %l/%L

let b:undo_ftplugin = 'setl stl<'

nnoremap <silent><buffer> S :sort i<bar>sort ir /[^\/]*$/<cr>
nnoremap <silent><buffer> D :silent keeppatterns g@\v/\.[^\/]+/?$@d_<cr>

nmap <buffer> h <plug>(dirvish_up)
noremap <silent><buffer> l :call dirvish#open('edit', 0)<cr>
