compiler javac
setlocal makeprg+=\ %

nnoremap <silent><buffer> \i
      \ :<c-u>call custom#Preserve('0;''}- sort /^import /')<cr>
