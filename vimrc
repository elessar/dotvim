set encoding=utf-8
scriptencoding utf-8

" GUI {{{1
if has('gui_running')
  set guioptions=M

  if has('win32')
    set guifont=Consolas:h10
    if has('directx')
      set renderoptions=type:directx
    endif
  endif
endif

" Options {{{1
if empty($XDG_DATA_HOME)
  let $XDG_DATA_HOME = expand('~/.local/share')
endif

function! s:CheckAndMakeDirectory(...)
  let l:printf_format = join(repeat(['%s'], a:0), '/')
  let l:printf_args = [l:printf_format] + a:000
  let l:path = call('printf', l:printf_args)
  call mkdir(l:path, 'p')
endfunction
let s:MkDataDir = funcref('s:CheckAndMakeDirectory', [$XDG_DATA_HOME, 'vim'])
call s:MkDataDir('swap')
call s:MkDataDir('undo')

set directory=$XDG_DATA_HOME/vim/swap//
set undofile undodir=$XDG_DATA_HOME/vim/undo
set viminfo+=n$XDG_DATA_HOME/vim/viminfo

setglobal autoread
set confirm
if has('patch-8.0.1361')
  set diffopt+=hiddenoff
endif
set display+=lastline
set foldmethod=indent foldlevelstart=99
set hidden
set history=200
set lazyredraw
set list listchars=tab:>-,trail:-,extends:>,precedes:<,nbsp:+
set mouse=a
set ruler rulerformat=%25(%=%l/%L\ %7(%c%V%)%)
set scrolloff=1
set sessionoptions-=options
set shortmess=atIF
set showcmd
set sidescroll=1 " Side-scroll by column
set sidescrolloff=5
setglobal statusline=%<%f%(\ %h%m%r%)%=\ %l/%L\ %7(%c%V%)
set switchbuf=useopen
set title
set ttimeout ttimeoutlen=50
set ttymouse=sgr " Allows mouse beyond the 223rd column
set visualbell t_vb=

" Wrapping
set breakindent   " Wrapped lines stay indented
set linebreak     " Preserve words when wrapping
set showbreak=>\  " String displayed before wrapped lines

" Editing
set backspace=indent,eol,start
set complete-=i        " Don't scan included files for completion
set completeopt+=menuone,noinsert
set formatoptions+=jro " Delete comment leader when joining
set nrformats-=octal
set showfulltag
setglobal tags=./tags;
set virtualedit=block

" Indentation
set autoindent
set expandtab shiftwidth=4
set shiftround     " Round > to multiples of 'shiftwidth'
set smarttab       " Use shiftwidth at the start of the line
set softtabstop=-1 " Use shiftwidth

" Wildmenu
set suffixes+=.jpg,.jpeg,.bmp,.gif,.png
set wildignore+=*.a,*.o,*.obj,*.exe,*.dll,*.manifest
set wildignore+=*.luac,*.pyc,*.pyo,*.class
set wildignore+=*.swp,*.bak,*.cache,*.pdb,*.min.*
set wildignorecase
set wildmenu
set wildmode=longest:full,full
set wildoptions=tagfile

" Searching
set hlsearch incsearch
set ignorecase smartcase
if executable('rg')
  set grepprg=rg\ --vimgrep\ --no-heading\ -S
  set grepformat=%f:%l:%c:%m
elseif executable('grep')
  set grepprg=grep\ -nRI
endif

" Autocommands
augroup WinSaveView
  autocmd!
  autocmd BufRead * unlet! b:win_save
  autocmd BufWinLeave * let b:win_save = winsaveview()
  autocmd BufWinEnter * call winrestview(get(b:, 'win_save', {}))
augroup END

augroup PreviewWindow
  autocmd!
  autocmd InsertLeave * if bufname('%') !=# '[Command Line]' | pclose | endif
augroup END

" Plugins {{{1
if !exists('g:user_runtime')
  let g:user_runtime = fnamemodify(expand('$MYVIMRC'), ':p:h')
endif

" Dirvish
let g:dirvish_relative_paths = 1
nmap <space>- <plug>(dirvish_up)
nnoremap <silent> <space>_ :<c-u>Dirvish<cr>

" Neosnippet
let g:neosnippet#snippets_directory = g:user_runtime . '/snips'
let g:neosnippet#disable_runtime_snippets = { '_': 1 }

" Matchit
if !exists('g:loaded_matchit')
  packadd! matchit
endif

" Filetype/Syntax
filetype plugin on
if !exists('g:syntax_on')
  syntax enable
endif

" Colors
if !exists('g:colors_name')
  silent! colorscheme el
endif
nnoremap <silent> <f4> :<c-u>execute custom#colors#CycleDark()<cr>
