function! s:Echo(group, message)
  execute 'echohl' a:group
  echo a:message
  echohl None
endfunction

function! custom#echo#Hl(group, ...)
  call s:Echo(a:group, join(a:000))
endfunction

function! custom#echo#Error(...)
  let l:message = join(a:000)
  call s:Echo('Error', l:message)
  let v:errmsg = a:msg
endfunction
