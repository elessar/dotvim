function! custom#git#Grep(arg)
  let l:grepprg_save = &grepprg
  let l:grepformat_save = &grepformat

  let &grepprg = 'git grep -nI'
  let &grepformat = '%f:%l:%m'
  execute 'silent! grep!'
        \ escape(matchstr(a:arg, '\v\C.{-}%($|[''" ]\@=\|)@='), '|')

  let &grepprg = l:grepprg_save
  let &grepformat = l:grepformat_save
endfunction

function! custom#git#LsFiles()
  let l:files = systemlist('git ls-files -mo --exclude-standard')
  if !v:shell_error && !empty(l:files)
    execute 'args' join(l:files)
  endif
endfunction
