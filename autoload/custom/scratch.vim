function! custom#scratch#Scratch(bang, mods)
  if !empty(bufname('%')) || a:bang
    if !empty(a:mods)
      execute a:mods 'new'
    elseif winwidth(0) > 160
      vnew
    else
      new
    endif
  endif

  setlocal buftype=nofile bufhidden=hide noswapfile
endfunction
