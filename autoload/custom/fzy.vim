" a:sink is a FuncRef accepting a filename argument
" Remaining arguments are passed to systemlist()
function! s:Fzy(sink, ...)
  try
    let l:output = call('systemlist', a:000)
  catch /Vim:Interrupt/
    " Swallow errors from ^C, to allow :redraw!
  endtry
  redraw!
  if !v:shell_error && !empty(l:output)
    return a:sink(join(l:output))
  endif
  return ''
endfunction

" Sinks
function! s:ProcessFilename(file)
  return fnameescape(fnamemodify(a:file, ':~:.'))
endfunction

function! s:FileSink(cmd, file)
  return a:cmd . ' ' . s:ProcessFilename(a:file)
endfunction

function! s:GetFileSink(cmd)
  return funcref('s:FileSink', [empty(a:cmd)? 'edit' : a:cmd])
endfunction

" Files
function! s:GetFileProvider()
  let l:fzy = ' | fzy -p "%s> "'
  if executable('fd')
    return 'fd --color never -uu' . printf(l:fzy, 'fd')
  elseif executable('rg')
    return 'rg --color never -uu --files' . printf(l:fzy, 'rg')
  endif

  return 'find -type f' . printf(l:fzy, 'find')
endfunction

function! custom#fzy#Files(cmd)
  return s:Fzy(s:GetFileSink(a:cmd), s:GetFileProvider())
endfunction
