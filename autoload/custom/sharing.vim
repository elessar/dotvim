function! custom#sharing#Share(line1, line2)
  let l:bin = systemlist('curl -sF "sprunge=<-" http://sprunge.us',
        \                getline(a:line1, a:line2))
  if has('clipboard')
    call setreg('+', l:bin, 'c')
  elseif has('win32unix') " Cygwin
    call writefile(l:bin, '/dev/clipboard', 'b')
  else
    echomsg join(l:bin)
    call custom#echo#Hl('WarningMsg', 'Not copied to clipboard')
  endif
  echo join(l:bin)
endfunction
