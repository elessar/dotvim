function! custom#diff#Orig(mods)
  diffthis

  if !empty(a:mods)
    execute a:mods 'new'
  elseif winwidth(0) < 160
    leftabove new
  else
    leftabove vnew
  endif
  setlocal buftype=nofile bufhidden=wipe
  silent file diff:#

  call setline(1, readfile(expand('#')))
  let &filetype = getbufvar('#', '&filetype')

  diffthis
  augroup MyDiffOrig
    autocmd! BufWipeout <buffer> diffoff!
  augroup END
endfunction

