function! s:HideBuffer(buffer)
  let l:original = win_getid()
  let l:buffer = bufnr(a:buffer)
  for l:window in win_findbuf(l:buffer)
    call win_gotoid(l:window)

    if buflisted(bufnr('#'))
      silent! buffer #
    else
      silent! bprevious
      if bufnr('%') == l:buffer
        enew
      endif
    endif
  endfor
  call win_gotoid(l:original)
endfunction

function! custom#bbye#Bdelete(buffer1, buffer2, ...)
  if a:0
    let l:buffers = filter(copy(a:000), 'bufexists(v:val)')
  else
    let l:range = range(a:buffer1, max([a:buffer1, a:buffer2]))
    let l:buffers = filter(l:range, 'bufloaded(v:val) || buflisted(v:val)')
  endif
  for l:buffer in l:buffers
    call s:HideBuffer(l:buffer)
  endfor
  return 'confirm bdelete ' . join(l:buffers)
endfunction
