let s:options = {
      \   'exit_cb': {j,e -> e || custom#echo#Hl('MoreMsg', 'tags generated')},
      \   'err_cb':  {c,m -> custom#echo#Error(m)},
      \ }

function! custom#ctags#Generate(...)
  if !executable('ctags')
    call custom#echo#Error('ctags: command not found')
    return
  endif
  call job_start(call('s:GetCommand', a:000), s:options)
endfunction

function! s:GetCommand(...)
  return extend(['ctags', '--recurse', '--tag-relative=always'], a:000)
endfunction
