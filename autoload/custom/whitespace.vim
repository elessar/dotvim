function! custom#whitespace#CheckTrailing(bang)
  match TrailingSpaces /\v\s+%#@!$/
  let l:line = search('\s$', 'nw')
  if l:line
    call custom#echo#Hl('WarningMsg', 'Trailing space on line', l:line)
    if a:bang
      call search('\s\+$', 'sw')
    endif
  else
    call custom#echo#Hl('MoreMsg', 'No whitespace errors')
  endif
endfunction
