let s:xdg_data = exists('$XDG_DATA_HOME')? '~/.local/share' : $XDG_DATA_HOME
let s:sessions_dir = expand(s:xdg_data) . '/vim/sessions/'
if !isdirectory(s:sessions_dir)
  call mkdir(s:sessions_dir, 'p')
endif

function! s:SessionName(session)
  return s:sessions_dir . a:session . '.vim'
endfunction

function! custom#sessions#GetSession(arg)
  return fnameescape(s:SessionName(a:arg))
endfunction

function! custom#sessions#SaveSession()
  if empty(v:this_session)
    call custom#Error('No active session')
    return
  endif
  execute 'mksession!' v:this_session
endfunction

function! custom#sessions#DropSession(arg)
  if empty(a:arg)
    if empty(v:this_session)
      call custom#echo#Hl('WarningMsg', 'No active session')
    else
      call delete(v:this_session)
      let v:this_session = ''
    endif
  else
    call delete(s:SessionName(a:arg))
  endif
endfunction

function! custom#sessions#Sessions(ArgLead, CmdLine, CursorPos)
  let l:sessions = getcompletion(s:SessionName('*'), 'file')
  call map(l:sessions, {i,v -> fnamemodify(v, ':t:r')})
  return join(l:sessions, "\n")
endfunction
