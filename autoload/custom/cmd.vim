function! custom#cmd#ExpandPath()
  let l:head = custom#cmd#GetDirectory()
  let l:tail = fnameescape(expand('%:t'))
  let l:cmdline = getcmdline()[: getcmdpos()-2]
  if l:cmdline[-len(l:tail) :] ==# l:tail
    return ''
  elseif l:cmdline[-len(l:head) :] ==# l:head || empty(l:head)
    return l:tail
  endif
  return l:head
endfunction

function! custom#cmd#GetDirectory()
  let l:head = expand('%:h')
  if empty(l:head) || l:head == '.'
    return ''
  endif
  return fnameescape(l:head) . '/'
endfunction

function! custom#cmd#ExpandExtension()
  let l:extension = expand('%:e')
  if empty(l:extension)
    return ''
  endif
  return '.' . l:extension
endfunction
