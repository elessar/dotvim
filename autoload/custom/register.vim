function! custom#register#Edit(register)
  let @= = histget('=')
  return printf('let @%s = %s', a:register, string(getreg(a:register)))
endfunction
