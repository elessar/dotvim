function! custom#textobjects#Object(function_name, ...)
  let l:search_save = getcharsearch()
  call call(a:function_name, a:000)
  call setcharsearch(l:search_save)
endfunction

function! s:NotFound(...)
  return call('match', insert(copy(a:000), getline('.'))) == -1
endfunction

function! s:SeekSeparator(char)
  let l:line = getline('.')
  let l:coln = col('.')
  let l:target = '\V' . escape(a:char, '\')
  if match(l:line, l:target) >= l:coln
    execute 'normal! f' . a:char
  elseif match(l:line, l:target, l:coln - 1) == -1
    execute 'normal! 2F' . a:char
  elseif l:line[l:coln - 1] !=# a:char || match(l:line, l:target, l:coln) == -1
    execute 'normal! F' . a:char
  endif
endfunction

" ia aa {{{1
function! custom#textobjects#InLeftArgument()
  call search(',', 'cW')
  call search('\S', 'bW')
  normal! v
  call search('(\s*\S', 'becW')
  normal! o
endfunction

function! custom#textobjects#AroundLeftArgument()
  call search(',\s*', 'ecW')
  normal! v
  call search('(.', 'becW')
  normal! o
endfunction

"  iA aA {{{1
function! custom#textobjects#InRightArgument()
  call search(',', 'bcW')
  call search('\S', 'W')
  normal! v
  call search('\S\s*)', 'cW')
endfunction

function! custom#textobjects#AroundRightArgument()
  call search('\s*,', 'bcW')
  normal! v
  call search('.)', 'cW')
endfunction

" Separator objects: i* a* {{{1
function! custom#textobjects#In(char)
  if s:NotFound(a:char, 0, 2)
    return
  endif

  call s:SeekSeparator(a:char)
  call search('\S', 'W')
  execute 'normal! vt' . a:char
  call search('\S', 'bcW')
endfunction

function! custom#textobjects#Around(char)
  if s:NotFound(a:char, 0, 2)
    return
  endif

  call s:SeekSeparator(a:char)
  execute 'normal! vt' . a:char
endfunction

" Seek {{{1
function! custom#textobjects#SeekIn(char)
  call s:Seek('i', a:char)
endfunction

function! custom#textobjects#SeekAround(char)
  call s:Seek('a', a:char)
endfunction

function! s:Seek(type, char)
  if s:NotFound(a:char)
    return
  endif

  execute 'normal!' printf('f%.1sv%s%s', a:char, a:type, a:char)
endfunction
