function! custom#syntax#SynStack(line, col)
  let l:id = synID(a:line, a:col, 1)
  if l:id
    echo   ':highlight' synIDattr(           l:id , 'name')
    execute 'highlight' synIDattr(synIDtrans(l:id), 'name')
  endif
endfunction
