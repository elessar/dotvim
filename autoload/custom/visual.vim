function! s:GetSelection()
  let [l:line1, l:col1, l:offset1] = getpos("'<")[1:]
  let [l:line2, l:col2, l:offset2] = getpos("'>")[1:]
  let [l:col1, l:col2] += [l:offset1 - 1, l:offset2 - 1]

  let l:mode = visualmode()
  let l:lines = getline(l:line1, l:line2)

  if l:mode ==# 'v'
    let l:lastlength = len(l:lines[-1])
    let l:lines[-1] = l:lines[-1][: l:col2]
    let l:lines[0] = l:lines[0][l:col1 :]
    if l:col2 >= l:lastlength
      call add(l:lines, '')
    endif
  elseif l:mode ==# 'V'
    call add(l:lines, '')
  elseif l:mode == "\<c-v>"
    let [l:col1, l:col2] = sort([l:col1, l:col2])
    call map(l:lines, 'v:val[col1 : col2]')
  endif
  return l:lines
endfunction

function! custom#visual#GetSelection()
  return join(s:GetSelection(), "\n")
endfunction

function! custom#visual#VisualStar()
  let l:selection = map(s:GetSelection(), {i,v -> escape(v, '/\')})
  let @/ = '\V' . join(l:selection, '\n')
endfunction
