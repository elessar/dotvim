let s:colors_dark = [
      \   'el',
      \   'apprentice',
      \ ]

function! s:Cycle(colors) abort
  let l:index = index(a:colors, get(g:, 'colors_name', '')) + 1
  return printf('colorscheme %s', a:colors[l:index % len(a:colors)])
endfunction

function! custom#colors#CycleDark()
  return s:Cycle(s:colors_dark)
endfunction
