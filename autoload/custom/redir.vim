function! custom#redir#ToBuffer(cmd, mods) abort
  let l:output = execute(a:cmd)

  if empty(a:mods)
    tabnew
  else
    execute a:mods 'new'
  endif
  setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted

  call setline(1, split(l:output, '\n'))
  let l:buffer_name = ':' . fnameescape(a:cmd)
  try
    execute 'file' l:buffer_name
  catch /^Vim(file):E95:/
    execute 'file'
          \ printf('%s (%d)', l:buffer_name, bufnr('%'))
  endtry
endfunction
