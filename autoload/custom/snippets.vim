let s:map_snippets = {i,v -> {
      \ 'word': v.real_name,
      \ 'menu': v.menu_abbr,
      \ }}

function! custom#snippets#Completion()
  if exists('g:loaded_neosnippet')
    let l:col = col('.')
    let l:trigger = escape(matchstr(getline('.'), '\S*\%' . l:col . 'c'), '\')

    let l:snippets = values(neosnippet#helpers#get_completion_snippets())
    call filter(l:snippets, {i,v -> v.real_name =~# '^\V' . l:trigger})
    call map(l:snippets, s:map_snippets)
    call complete(l:col - len(l:trigger), l:snippets)
  endif
  let @= = histget('=')
  return ''
endfunction
