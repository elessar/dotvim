function! custom#Grep(...)
  execute 'silent! grep!' join(map(copy(a:000), 'fnameescape(v:val)'))
endfunction

function! custom#Preserve(cmd)
  let l:view_save = winsaveview()
  execute 'keepjumps keeppatterns' a:cmd
  call winrestview(l:view_save)
endfunction
