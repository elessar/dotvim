if exists('g:loaded_unimpaired')
  finish
endif
let g:loaded_unimpaired = 1

" *{next,prev} maps
function! s:CreateMapPairs(map_char, command_char)
  execute printf('nnoremap <silent> [%s :<c-u>execute v:count1 . ''%sprevious''<cr>',
        \        a:map_char, a:command_char)
  execute printf('nnoremap <silent> ]%s :<c-u>execute v:count1 . ''%snext''<cr>',
        \        a:map_char, a:command_char)

  let l:upper = toupper(a:map_char)
  execute printf('nnoremap <silent> [%s :<c-u>%sfirst<cr>',
        \        l:upper, a:command_char)
  execute printf('nnoremap <silent> ]%s :<c-u>%slast<cr>',
        \        l:upper, a:command_char)
endfunction
call s:CreateMapPairs('a', '')
call s:CreateMapPairs('b', 'b')
call s:CreateMapPairs('q', 'c')
call s:CreateMapPairs('l', 'l')


" Toggle booleans
nnoremap <silent> coh :<c-u>set hlsearch! hlsearch?<cr>
nnoremap <silent> col :<c-u>setlocal list! list?<cr>
nnoremap <silent> cos :<c-u>setlocal spell! spell?<cr>
nnoremap <silent> cow :<c-u>setlocal wrap! wrap?<cr>
nnoremap <silent> cor :<c-u>setlocal relativenumber! relativenumber?<cr>
nnoremap <silent> con :<c-u>setlocal number! number?<cr>

" Toggle lines
nnoremap <silent> coc :<c-u>setlocal cursorline!<cr>
nnoremap <silent> coC :<c-u>call <sid>ToggleColumn()<cr>

function! s:ToggleColumn()
  if empty(&colorcolumn)
    setlocal colorcolumn=+1 colorcolumn?
  else
    setlocal colorcolumn= colorcolumn?
  endif
endfunction
