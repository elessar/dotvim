if exists('g:loaded_myMaps')
  finish
endif
let g:loaded_myMaps = 1

map <space>   <nop>
map <s-space> <space>

" j and k move by a display line unless a count is used
nnoremap <expr> j (v:count? 'j' : 'gj')
xnoremap <expr> j (v:count? 'j' : 'gj')
nnoremap <expr> k (v:count? 'k' : 'gk')
xnoremap <expr> k (v:count? 'k' : 'gk')

" Save file
nnoremap <silent> <space>w :<c-u>update<cr>

" Make
nnoremap <silent> <f5> :<c-u>silent make!<bar>cwindow<bar>redraw!<cr>

" Y yanks to end of line, like D & C, rather than yy
nnoremap Y y$

" Jump to mark's column
nnoremap ' `
xnoremap ' `

" Select last changed block
nnoremap gV `[vg`]

" Edit alternate file
nnoremap g<bs> <c-^>

" Clear search highlight
nnoremap <silent> <bs> :<c-u>nohlsearch<cr>
xnoremap <silent> <bs> :<c-u>nohlsearch<cr>gv

" Visual star
xnoremap <silent> * :<c-u>call custom#visual#VisualStar()<cr>n
xnoremap <silent> # :<c-u>call custom#visual#VisualStar()<bar>
      \ let v:searchforward = 0<cr>n

" cgn word under cursor
nnoremap c* *``cgn
nnoremap c# #``cgN

" Replace word under cursor
nnoremap c&& :<c-u>'{,'}s/\<<c-r><c-w>\>//gc<c-f>F/<c-c>
nnoremap c&%     :<c-u>%s/\<<c-r><c-w>\>//gc<c-f>F/<c-c>

" gs to split lines (go split), the opposite of J to join
nnoremap <silent> gs :<c-u>keeppatterns s/\s*\%#\s*/\r/<cr>

" Edit a register
nnoremap c" :<c-u><c-r><c-r>=custom#register#Edit(v:register)<cr><c-f><left>

" Reformat entire file
nnoremap <silent> g== :<c-u>call custom#Preserve('normal! gg=G')<cr>

" Expands current file
cnoremap <expr> <sid>(Directory) custom#cmd#GetDirectory()
cnoremap <expr> <sid>(ExpandPath) custom#cmd#ExpandPath()
cnoremap <script> <c-o> <sid>(ExpandPath)

" Inserts **/*
cnoremap <c-s> **/*

" Inserts extension
cnoremap <expr> <c-x> custom#cmd#ExpandExtension()

" Break undo sequences
inoremap <c-u> <c-g>u<c-u>

" Snippet expansion
imap <c-b> <plug>(neosnippet_expand_or_jump)
smap <c-b> <plug>(neosnippet_expand_or_jump)

" Snippet completion
inoremap <silent> <c-r><tab> <c-r>=custom#snippets#Completion()<cr>

" Accept ins-completion menu
inoremap <expr> <cr> (pumvisible() ? '<c-y>' : '<cr>')

" Buffers
nnoremap gb :<c-u>ls<cr>:silent buffer<space>

" Navigation
function! s:SetupWildcardMaps(key, command)
  execute printf('nnoremap <script> <space>%s :<c-u>%s <sid>(Directory)**/*',
        \ a:key, a:command)
  execute printf('nnoremap <space>%s :<c-u>%s **/*',
        \ toupper(a:key), a:command)
endfunction

call s:SetupWildcardMaps('e', 'edit')
call s:SetupWildcardMaps('f', 'find')
call s:SetupWildcardMaps('a', 'argadd')
call s:SetupWildcardMaps('n', 'next')

" History
nnoremap <space>r :<c-u>browse filter /\c/ oldfiles<c-f>2b<c-c>

" Fzy
if !has('gui_running') && executable('fzy')
  command! -nargs=? -complete=command Fzy execute custom#fzy#Files(<q-args>)

  nnoremap <space>p :<c-u>Fzy<cr>
else
  nnoremap <silent> <space>p :<c-u>echo 'Sorry, Fzy not available.'<cr>
endif

" Tag jump
nnoremap <space>] :<c-u>tjump /

" Include search
nnoremap <space>i :Ilist<space>
xnoremap <space>i :Ilist<space>
nnoremap <space>d :Dlist<space>
xnoremap <space>d :Dlist<space>

" Git
nnoremap <silent> gA :<c-u>call custom#git#LsFiles()<cr>
