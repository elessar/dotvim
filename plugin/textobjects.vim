if exists('g:loaded_myTextObjects')
  finish
endif
let g:loaded_myTextObjects = 1

xnoremap <silent> il :<c-u>execute 'normal! ^v' . v:count1 . 'g_'<cr>
onoremap <silent> il :<c-u>execute 'normal! ^v' . v:count1 . 'g_'<cr>
xnoremap <silent> al :<c-u>execute 'normal! 0v' . v:count1 . '$h'<cr>
onoremap <silent> al :<c-u>execute 'normal! 0v' . v:count1 . '$h'<cr>
xnoremap <silent> ie :<c-u>execute 'normal! G0V'<bar>1<cr>
onoremap <silent> ie :<c-u>execute 'normal! G0V'<bar>1<cr>

" Arguments
let s:argument_functions = {
      \   'ia': 'custom#textobjects#InLeftArgument',
      \   'aa': 'custom#textobjects#AroundLeftArgument',
      \   'iA': 'custom#textobjects#InRightArgument',
      \   'aA': 'custom#textobjects#AroundRightArgument',
      \ }

let s:argument_format = ':<c-u>call custom#textobjects#Object(''%s'')<cr>'
for [s:object, s:function] in items(s:argument_functions)
  let s:function = printf(s:argument_format, s:function)
  execute 'xnoremap <silent>' s:object s:function
  execute 'onoremap <silent>' s:object s:function
endfor

" Separators (include only one edge in the 'a' variant)
function! s:MapSeparators(char, in_function, around_function)
  let l:format = ':<c-u>call custom#textobjects#Object(''%s'', ''%s'')<cr>'
  let l:in = printf(l:format, a:in_function, a:char)
  let l:around = printf(l:format, a:around_function, a:char)
  execute 'xnoremap <silent> i' . a:char l:in
  execute 'onoremap <silent> i' . a:char l:in
  execute 'xnoremap <silent> a' . a:char l:around
  execute 'onoremap <silent> a' . a:char l:around
endfunction

let s:in_format = 'custom#textobjects#In'
let s:around_format = 'custom#textobjects#Around'
let s:separators =
      \ [',', '.', ';', ':', '+', '-', '=', '~', '_',
      \  '*', '#', '/', '<bar>', '\', '&', '$']
for s:char in s:separators
  call s:MapSeparators(s:char, s:in_format, s:around_format)
endfor

" Seek
function! s:MapSeeks(map, search)
  let l:format = 'noremap <silent> %sn%s ' .
        \ ':<c-u>call custom#textobjects#Object(''%s'', ''%s'')<cr>'
  let l:in = printf(l:format,
        \ 'i', a:map, 'custom#textobjects#SeekIn', a:search)
  let l:around = printf(l:format,
        \ 'a', a:map, 'custom#textobjects#SeekAround', a:search)
  execute 'x' . l:in
  execute 'o' . l:in
  execute 'x' . l:around
  execute 'o' . l:around
endfunction

call s:MapSeeks('b', '(')
call s:MapSeeks('B', '{')
call s:MapSeeks('[', '[')
call s:MapSeeks(']', '[')
call s:MapSeeks('<', '<')
call s:MapSeeks('>', '<')
