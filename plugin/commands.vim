if exists('g:loaded_myCommands')
  finish
endif
let g:loaded_myCommands = 1

" :Secret
command! Secret set noswapfile noundofile viminfo=

" :Bdelete
command! -range -addr=buffers -nargs=* -complete=buffer -bar Bdelete
      \ execute custom#bbye#Bdelete(<line1>, <line2>, <f-args>)

" :CheckSpaces
command! -bang -bar CheckSpaces call custom#whitespace#CheckTrailing(<bang>0)
highlight default link TrailingSpaces Error

command! -range=% -bar TrimSpaces
      \ silent call custom#Preserve('<line1>,<line2>s/\s\+$//e')

" :Ctags
command! -nargs=* -complete=file Ctags call custom#ctags#Generate(<f-args>)

" :DiffOrig
command! -bar DiffOrig call custom#diff#Orig(<q-mods>)

" :G
command! -nargs=1 G !git <args>

" :Grep
command! -nargs=+ -complete=file Grep call custom#Grep(<f-args>) | redraw!

command! -nargs=+ -complete=file Ggrep
      \ call custom#git#Grep(<q-args>) | redraw!

" :Mksession
command! -nargs=1 -complete=custom,custom#sessions#Sessions -bang -bar
      \ Mksession execute 'mksession<bang>'
      \ custom#sessions#GetSession(<q-args>)

command! -nargs=1 -complete=custom,custom#sessions#Sessions -bar
      \ LoadSession execute 'source' custom#sessions#GetSession(<q-args>)

command! -bar SaveSession call custom#sessions#SaveSession()

command! -nargs=? -complete=custom,custom#sessions#Sessions -bar
      \ DropSession call custom#sessions#DropSession(<q-args>)

" :Redir
command! -nargs=1 -complete=command Redir
      \ call custom#redir#ToBuffer(<q-args>, <q-mods>)

" :Scratch
command! -bang -bar Scratch call custom#scratch#Scratch(<bang>0, <q-mods>)

" :Share
command! -range=% -bar Share call custom#sharing#Share(<line1>, <line2>)

" :SynStack
command! -bar SynStack call custom#syntax#SynStack(line('.'), col('.'))
